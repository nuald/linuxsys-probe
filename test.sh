#!/bin/sh

test_host() {
    zig build run -- --delta=1GiB -n
}

test_host_taskset() {
    taskset -c 2-4 zig build run -- -d 1GiB
}

test_qemu() {
    zig build
    cd zig-out/x86_64-linux-musl/
    # tag::test-qemu[]
    qemu-x86_64 -R 100M ./linuxsys-probe -r probe.mem
    # end::test-qemu[]
    cd -
}

test_docker() {
    zig build
    cd zig-out/x86_64-linux-musl/
    # tag::test-docker[]
    docker run -it --rm -v `pwd`:/opt \
           --cpuset-cpus="2-4" --memory="100m" alpine \
           /opt/linuxsys-probe -d 10MiB -r probe
    # end::test-docker[]
    cd -
}

test_docker_arm64() {
    zig build
    cd zig-out/aarch64-linux-musl/
    # tag::test-docker-arm64[]
    docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    docker run -it --rm -v `pwd`:/opt --platform linux/arm64 \
           --cpuset-cpus="2-4" --memory="100m" arm64v8/alpine \
           /opt/linuxsys-probe -d 10MiB -v -n
    # end::test-docker-arm64[]
    cd -
}

test_docker_arm32() {
    zig build
    cd zig-out/arm-linux-musleabi/
    docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    docker run -it --rm -v `pwd`:/opt --platform linux/arm/v7 \
           --cpuset-cpus="2-4" --memory="100m" arm32v7/alpine \
           /opt/linuxsys-probe -d 10MiB
    cd -
}

start_lxc() {
    zig build
    cd zig-out/x86_64-linux-musl/
    # tag::test-lxc[]
    sudo lxc-create -n alpine -t download -- \
         --dist alpine --release edge --arch amd64
    sudo lxc-start -n alpine -s "lxc.mount.entry=`pwd` opt none bind 0 0"
    sudo lxc-cgroup -n alpine cpuset.cpus 2-4
    sudo lxc-cgroup -n alpine memory.max 100m
    sudo lxc-attach -n alpine -- /opt/linuxsys-probe -v
    # end::test-lxc[]
    cd -
}

destroy_lxc() {
    # tag::test-lxc[]
    sudo lxc-destroy -n alpine -f
    # end::test-lxc[]
}

test_lxc() {
    start_lxc
    destroy_lxc
}

test_lxc_external() {
    start_lxc
    sudo zig-out/x86_64-linux-musl/linuxsys-probe -v -p lxc:alpine -r cgroup
    destroy_lxc
}

test_panic() {
    zig build
    sudo sysctl vm.panic_on_oom=1
    qemu-x86_64 -R 100M zig-out/x86_64-linux-musl/linuxsys-probe -v
    qemu-x86_64 -R 100M zig-out/x86_64-linux-musl/linuxsys-probe -f
    sudo sysctl vm.panic_on_oom=0
}

test_all() {
    # tag::test-all[]
    test_host            # test on host
    test_host_taskset    # test on host with taskset applied
    test_qemu            # test in QEMU
    test_docker          # test in Docker, x86_64
    test_docker_arm64    # test in Docker, aarch64
    test_docker_arm32    # test in Docker, arm32v7
    test_lxc             # test in LXC
    test_lxc_external    # test on host the LXC init process
    test_panic           # test in QEMU with vm.panic_on_oom=1
    # end::test-all[]
}

if [ "_$1" = "_" ]; then
    test_all
else
    "$@"
fi
