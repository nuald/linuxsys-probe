const bytes = @import("bytes.zig");
const c = @import("c.zig");

const std = @import("std");
const fmt = std.fmt;
const mem = std.mem;
const os = std.os;
const testing = std.testing;

const Allocator = mem.Allocator;
const ChildProcess = std.ChildProcess;
const Str = []const u8;

const default_delta = "1MiB";

const long_options = [_]c.option{
    c.option{
        .name = "verbose",
        .has_arg = c.no_argument,
        .flag = 0,
        .val = 'v',
    },
    c.option{
        .name = "force",
        .has_arg = c.no_argument,
        .flag = 0,
        .val = 'f',
    },
    c.option{
        .name = "no-humanize",
        .has_arg = c.no_argument,
        .flag = 0,
        .val = 'H',
    },
    c.option{
        .name = "interval",
        .has_arg = c.required_argument,
        .flag = 0,
        .val = 'i',
    },
    c.option{
        .name = "notes",
        .has_arg = c.no_argument,
        .flag = 0,
        .val = 'n',
    },
    c.option{
        .name = "run",
        .has_arg = c.required_argument,
        .flag = 0,
        .val = 'r',
    },
    c.option{
        .name = "delta",
        .has_arg = c.required_argument,
        .flag = 0,
        .val = 'd',
    },
    c.option{
        .name = "pid",
        .has_arg = c.required_argument,
        .flag = 0,
        .val = 'p',
    },
    c.option{ .name = 0, .has_arg = 0, .flag = 0, .val = 0 },
};

const usage =
    \\Usage: {s} [options]
    \\
    \\Options:
    \\    -d, --delta DELTA   set delta for memory probe ({s} by default)
    \\    -f, --force         enforce memory probe
    \\    -i, --interval INTERVAL
    \\                        refresh interval in seconds (no refresh by default)
    \\    -H, ----no-humanize
    \\                        shows stats as raw numbers, not humanized
    \\    -n, --notes         show notes about the elements
    \\    -p, --pid [PID|lxc:CONTAINER]
    \\                        set pid for probes (self by default)
    \\    -r, --run PROBES    run the specified probes only
    \\    -v, --verbose       enable verbose output
    \\    -h, --help          print this help menu
    \\
    \\NOTE: Please use Enter to exit refresh mode.
    \\
;

const OptionsError = error{UnknownOption};

pub const OptStr = struct {
    const Self = @This();

    allocator: *Allocator,
    default: Str,
    value: ?Str = null,

    pub fn init(alloc: *Allocator, default: Str) Self {
        return Self{
            .allocator = alloc,
            .default = default,
        };
    }

    pub fn deinit(self: Self) void {
        if (self.value) |unwrapped| {
            self.allocator.free(unwrapped);
        }
    }

    pub fn set(self: *Self, value: ?Str) void {
        self.deinit();
        self.value = value;
    }

    pub fn get(self: Self) Str {
        return self.value orelse self.default;
    }
};

pub const Options = struct {
    const Self = @This();

    allocator: *Allocator,
    force: bool = false,
    no_humanize: bool = false,
    mem_delta: u64,
    interval: ?u64 = null,
    verbose: bool = false,
    notes: bool = false,
    pid: OptStr,
    run: OptStr,

    fn getActualPid(alloc: *Allocator, pid_expr: Str) !Str {
        const lxc_prefix = "lxc:";
        var stdout: ?Str = null;
        defer if (stdout) |s| alloc.free(s);

        if (mem.startsWith(u8, pid_expr, lxc_prefix)) {
            const lxc_name = pid_expr[lxc_prefix.len..];
            const cmd = [_]Str{ "lxc-info", "-n", lxc_name, "-p", "-H" };

            const result = try ChildProcess.exec(
                .{ .allocator = alloc, .argv = cmd[0..] },
            );

            if (result.term == .Exited) {
                const slice = mem.trimRight(u8, result.stdout, "\n");
                return try alloc.dupeZ(u8, slice);
            }
        }
        return try alloc.dupeZ(u8, pid_expr);
    }

    pub fn init(alloc: *Allocator) !Options {
        return argInit(alloc, @intCast(c_int, os.argv.len), os.argv.ptr);
    }

    pub fn argInit(alloc: *Allocator, argc: c_int, argv: [*][*:0]u8) !Options {
        var self = Self{
            .allocator = alloc,
            .mem_delta = bytes.fromString(default_delta),
            .pid = OptStr.init(alloc, "self"),
            .run = OptStr.init(alloc, ""),
        };
        // Reset optind to be able to parse options again in unit-tests
        c.optind = 1;
        while (true) {
            switch (c.getopt_long(
                argc,
                argv,
                "Hvhfd:i:nr:p:",
                &long_options[0],
                0,
            )) {
                -1 => break,
                'f' => {
                    self.force = true;
                },
                'v' => {
                    self.verbose = true;
                },
                'H' => {
                    self.no_humanize = true;
                },
                'd' => {
                    self.mem_delta = bytes.fromString(mem.span(c.optarg));
                },
                'i' => {
                    self.interval = try fmt.parseInt(u64, mem.span(c.optarg), 10);
                },
                'n' => {
                    self.notes = true;
                },
                'r' => {
                    self.run.set(try alloc.dupeZ(u8, mem.span(c.optarg)));
                },
                'p' => {
                    self.pid.set(try getActualPid(alloc, mem.span(c.optarg)));
                },
                else => {
                    std.debug.print(usage, .{ argv[0], default_delta });
                    return OptionsError.UnknownOption;
                },
            }
        }
        self.verbose = self.verbose and !self.no_humanize;
        self.notes = self.notes and !self.no_humanize;
        return self;
    }

    pub fn deinit(self: Self) void {
        self.run.deinit();
        self.pid.deinit();
    }
};

test "empty options" {
    const alloc = testing.allocator;

    const empty_options = try Options.init(alloc);
    try testing.expectEqual(false, empty_options.force);
    try testing.expectEqual(false, empty_options.verbose);
    try testing.expectEqual(false, empty_options.no_humanize);
    try testing.expectEqual(false, empty_options.notes);
    try testing.expectEqual(empty_options.mem_delta, 1048576);
    try testing.expect(empty_options.interval == null);
    try testing.expectEqualStrings(empty_options.pid.get(), "self");
    try testing.expectEqualStrings(empty_options.run.get(), "");
    empty_options.deinit();
}

test "short options" {
    const alloc = testing.allocator;

    var short_argv = [_][*:0]const u8{
        "prog",
        "-f",
        "-v",
        "-H",
        "-n",
        "-d",
        "1GiB",
        "-i",
        "10",
        "-p",
        "123",
        "-r",
        "proc",
    };
    const short_options = try Options.argInit(
        alloc,
        short_argv.len,
        @ptrCast([*][*:0]u8, &short_argv),
    );
    try testing.expectEqual(true, short_options.force);
    try testing.expectEqual(false, short_options.verbose);
    try testing.expectEqual(true, short_options.no_humanize);
    try testing.expectEqual(false, short_options.notes);
    try testing.expectEqual(short_options.mem_delta, 1073741824);
    try testing.expectEqual(short_options.interval.?, 10);
    try testing.expectEqualStrings(short_options.pid.get(), "123");
    try testing.expectEqualStrings(short_options.run.get(), "proc");
    short_options.deinit();
}

test "long options" {
    const alloc = testing.allocator;

    var long_argv = [_][*:0]const u8{
        "prog",
        "--force",
        "--verbose",
        "--no-humanize",
        "--notes",
        "--delta=1GiB",
        "--interval=10",
        "--pid=123",
        "--run=proc",
    };
    const long_opts = try Options.argInit(
        alloc,
        long_argv.len,
        @ptrCast([*][*:0]u8, &long_argv),
    );
    try testing.expectEqual(true, long_opts.force);
    try testing.expectEqual(false, long_opts.verbose);
    try testing.expectEqual(true, long_opts.no_humanize);
    try testing.expectEqual(false, long_opts.notes);
    try testing.expectEqual(long_opts.mem_delta, 1073741824);
    try testing.expectEqual(long_opts.interval.?, 10);
    try testing.expectEqualStrings(long_opts.pid.get(), "123");
    try testing.expectEqualStrings(long_opts.run.get(), "proc");
    long_opts.deinit();
}
