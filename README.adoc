= linuxsys-probe
:toc:
:source-highlighter: rouge

GNU/Linux system probe utility to:

* read cgroups information (cpusets and memory)
* read /proc information
* probe CPU affinity and memory allocation

== Use

----
Usage: ./linuxsys-probe [options]

Options:
    -d, --delta DELTA   set delta for memory probe (1024.00 KiB by default)
    -f, --force         enforce memory probe
    -i, --interval INTERVAL
                        refresh interval in seconds (no refresh by default)
    -H, ----no-humanize
                        show stats as raw numbers, not humanized
    -n, --notes         show notes about the elements
    -p, --pid [PID|lxc:CONTAINER]
                        set pid for probes (self by default)
    -r, --run PROBES    run the specified probes only
    -v, --verbose       enable verbose output
    -h, --help          print this help menu

NOTE: Please use Enter to exit refresh mode.
----

Available probes:

* `cgroup.v1.cpuset` - Shows cgroups-v1 cpuset information from `/sys/fs/cgroup` virtual file system
* `cgroup.v1.memory` - Shows cgroups-v1 memory information from `/sys/fs/cgroup` virtual file system
* `cgroup.v2.cpuset` - Shows cgroups-v2 cpuset information from `/sys/fs/cgroup` virtual file system
* `cgroup.v2.memory` - Shows cgroups-v2 memory information from `/sys/fs/cgroup` virtual file system
* `proc.cpuinfo` - Shows `/proc/cpuinfo` information
* `proc.stat` - Shows `/proc/<pid>/stat` information
* `proc.sys.vm` - Shows `/proc/sys/vm/` information
* `sysinfo` - Shows `sysinfo` API information
* `probe.cpu` - Probes CPU cores affinity
* `probe.mem` - Probes memory allocation

Options:

* `-d, --delta DELTA` - set the minimum delta between upper and lower bounds
for memory probe. As soon as the difference
between upper and lower bounds becomes less than this delta, the probing stops.
* `-f, --force` - enforce the memory probe. If `/proc/sys/vm/panic_on_oom`
contains value > 0, a kernel panic could occur during the memory probe.
As it's not safe, the utility doesn't proceed unless this option is enabled.
* `-i, --interval INTERVAL` - the utility starts in a loop mode until
Enter key is pressed.
No CPU affinity and memory probes are running in this mode.
* `-H, ----no-humanize`` - show stats as raw numbers, not humanized. Required
for futher machine analysis.
`--verbose` and `--notes` flags are ignored in that mode.
* `-n, --notes` - show notes about the elements
(e.g. the description of a cgroup key).
* `-v, --verbose` - enable verbose output that provides additional information
(e.g. the path to cgroup element).
* `-p, --pid [PID|lxc:CONTAINER]` - set PID of the process to analyze.
Applied to cgroups and procfs probes only. Accepts `self` (analize itself),
`<number>` (actual PID of the process), `lxc:CONTAINER`
(using PID of the `CONTAINER` collected by `lxc-info`).
* `-r, --run PROBES` - run the specified probes only, could be a substring
(e.g. `-r probe` refers to both `probe.cpu` and `probe.mem`,
and `-r probe.m` refers to `probe.mem`).
* `-h, --help` - print the help menu.

References:

 * https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v1/[The Linux Kernel: Control Group v1]
 * https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v2.html[The Linux Kernel: Control Group v2]
 * https://www.kernel.org/doc/html/latest/admin-guide/sysctl/vm.html[The Linux Kernel: /proc/sys/vm/]

=== Probe CPU affinity and memory allocation

Setting CPU affinity and allocating memory could silently fail (e.g. if cgroups
control them). The utility uses those failures to find the boundaries:

[source,sh,indent=0]
----
include::test.sh[tag=test-docker]
----
[source]
----
probe.cpu::affinity [0] = [*2, 3, 4]
probe.cpu::affinity [1] = [*2, 3, 4]
probe.cpu::affinity [2] = [*2]
probe.cpu::affinity [3] = [*3]
probe.cpu::affinity [4] = [*4]
probe.cpu::affinity [5] = [*2, 3, 4]
probe.cpu::affinity [6] = [2, *3, 4]
probe.cpu::affinity [7] = [2, 3, *4]
probe.cpu::affinity [8] = [2, 3, *4]
probe.cpu::affinity [9] = [2, *3, 4]
probe.cpu::affinity [10] = [*2, 3, 4]
probe.cpu::affinity [11] = [*2, 3, 4]
probe.mem::alloc 4.26 GiB = false
probe.mem::alloc 2.13 GiB = false
probe.mem::alloc 1.07 GiB = false
probe.mem::alloc 545.69 MiB = false
probe.mem::alloc 272.85 MiB = false
probe.mem::alloc 136.42 MiB = false
probe.mem::alloc 68.21 MiB = true
probe.mem::alloc 102.32 MiB = false
probe.mem::alloc 85.26 MiB = true
probe.mem::alloc 93.79 MiB = false
----

`*` marks the active CPU core during the probe.

=== Plot histogram from procfs values

The example uses https://github.com/glamp/bashplotlib[bashplotlib] python package:

[source,sh]
----
pip install bashplotlib
----

First, run the utility in refresh mode (please use Enter to stop it). Assuming
`60817` is PID of the process we examine:

[source.sh]
----
./linuxsys-probe -i 1 -r proc.stat -H -p 60817 | tee results.txt
----
[source]
----
proc.stat::pid 60817
proc.stat::comm actix-web
proc.stat::state S
proc.stat::ppid 56306
proc.stat::pgrp 60817
proc.stat::session 56291
proc.stat::tty_nr 34816
proc.stat::tpgid 60817
proc.stat::flags 4194304
proc.stat::minflt 573
proc.stat::cminflt 0
proc.stat::majflt 0
proc.stat::cmajflt 0
proc.stat::utime 0
proc.stat::stime 0
proc.stat::cutime 0
proc.stat::cstime 0
proc.stat::priority 20
proc.stat::nice 0
proc.stat::num_threads 14
proc.stat::starttime 172820
proc.stat::vsize 907780096
proc.stat::rss 4407296
...
----

Next, use `hist` to draw the histogram (grepping for RSS):

[source.sh]
----
grep "rss " results.txt | cut -d' ' -f2 | hist
----
[source]
----
 41|      o
 39|      o
 37|      o
 35|      o
 33|      o
 31|      o
 28|      o
 26| o    o
 24| o    o
 22| o    o
 20| o    o
 18| o    o
 16| o    o
 13| o    o
 11| o    o
  9| o    o
  7| o    o
  5| o    o    o
  3| o    o    o
  1| o    o   oo
    -----------

-----------------------------------
|             Summary             |
-----------------------------------
|         observations: 75        |
|    min value: 3801088.000000    |
|      mean : 10590999.893333     |
|    max value: 23162880.000000   |
-----------------------------------
----

=== Platform notes

==== QEMU

Probing guest virtual address space with QEMU User space emulator
(requires `qemu-arch-extra` on Arch Linux):

[source,sh,indent=0]
----
include::test.sh[tag=test-qemu]
----

References:

 * https://qemu-project.gitlab.io/qemu/user/main.html[QEMU User space emulator]

==== Docker

Probing everything with the full verbosity in Docker ARM64 platform:

[source,sh,indent=0]
----
include::test.sh[tag=test-docker-arm64]
----

References:

 * https://docs.docker.com/config/containers/resource_constraints/[Docker: Runtime options with Memory, CPUs, and GPUs]
 * https://docs.docker.com/config/containers/runmetrics/[Docker: Runtime metrics]


==== LXC

Probing everything with the verbosity in LXC platform:

[source,sh,indent=0]
----
include::test.sh[tag=test-lxc]
----

References:

* https://linuxcontainers.org/lxc/getting-started/[LXC: Getting started]

== Build

Requires https://ziglang.org/[Zig language], stable version (currently 0.8.1).

One-step build (currently for x86_64, ARM32v7 and ARM64 GNU/Linux), the output is in `zig-out` directory:

[source,sh]
----
zig build
----

=== Contribute

Please run tests to verify the proper working in various environments:

[source,sh]
----
zig build test -Dclean
----

Individual tests could be run with `./test.sh <test-name>` command.
The available tests are:

include::test.sh[tag=test-all]

=== Release

Release process is automated and requires only new Git tag added:

[source,sh]
----
git tag -a <tag> -m"<message>"
git push --tags
----

However, the releases could be build manually too:

[source,sh]
----
zig build dist -Dclean -Dversion="<version>"
----

The archives will be available in `target/dist`.
